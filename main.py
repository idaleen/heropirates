from flask import Flask, jsonify, request, make_response
import jwt
import datetime
import requests
import json
import os
from functools import wraps


app = Flask(__name__)

app.config['SECRET_KEY'] = 'thisisthesecretkey'


def token_required(f):
    @wraps(f)
    def decorated(*args,**kwargs):

        Bearer = None
        if 'token' in request.headers:
            Bearer = request.headers['token']
        

        if not Bearer:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            data = jwt.decode(Bearer, app.config['SECRET_KEY'])

        except:
            return jsonify({'error': 'Token is invalid'}), 401
        
        return f(*args, **kwargs)
    
    return decorated

def checkPirates(ar): #eyes
    count=0
    eyes = [';','8']
    nose =['-','~']
    mouth = [')','|']
    for item in ar:
        if len(item) == 2:
            if (item[0] in eyes and item[1] in mouth):
                count+=1
                
        if len(item) == 3:
            if (item[0] in eyes and item[1] in nose and item[2] and mouth):
                count+=1
    #result = {"piratesFound": count, "id":ids}
    return count


# route for unauthenticated users
@app.route('/unprotected')
def unprotected():
    return jsonify ({'message': 'Unauthorized'})


#route for authenticated users.
@app.route('/pirates/countPirates')
@token_required
def protected():
    #return jsonify ({'message': 'Authorized'})
    main_api = "https://eilapirate.herokuapp.com/pirates/prison"
    json_data = requests.get(main_api).json()
    ids = json_data['id']
    faces = list(json_data['faces'])

    output= checkPirates(faces)
    result = jsonify({"piratesFound":output, "id":ids})
    return result


@app.route('/login')
def login():
    auth = request.authorization

    if auth and auth.username =='user' and auth.password == 'password':
        Bearer = jwt.encode({'user': 'auth.username', 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])
        return jsonify({'Bearer': Bearer.decode('UTF-8')})
    return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm ="Login Required"'})

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
    #app.run(debug=True)